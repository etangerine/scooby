<?php

/*
* @Author 		ParaTheme
* Copyright: 	2015 ParaTheme
*/

if ( ! defined('ABSPATH')) exit;  // if direct access 
	
	$html.= '<div class="wcps-items-thumb"><a href="'.$permalink.'"><img alt="'.get_the_title().'" src="'.$wcps_thumb_url.'" /></a></div>';
	
