=== Image Caption Hover ===
Contributors: Rameez_Iqbal
Donate link: https://www.2checkout.com/checkout/purchase?sid=202485641&quantity=1&product_id=1
Tags: image, caption, widget, hover, css3, responsive, theme, thumbnail, sidebar, easy, simple, footer, image caption, upload, library, styles, wpml, multi language, i10n, i18n, german, french
Requires at least: 3.5
Tested up to: 4.3
Stable tag: 5.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add images anywhere in your site with 60+ caption hover effects.

== Description ==

Add images anywhere in your site with 60+ caption hover effects. It is Simple and Flexible.
<br><br>
<a href="http://webcodingplace.com/image-caption-hover-demo/">FREE VERSION DEMO</a><br>

<a href="http://webcodingplace.com/image-caption-hover-pro-demo/">PRO VERSION DEMO</a><br>

<a href="http://webcodingplace.com/image-caption-hover-wordpress-plugin/">Help and Support</a><br>

<a href="http://webcodingplace.com/image-caption-hover-pro-wordpress-plugin/">GET PRO</a><br>

<h3>Translations</h3>
<ul>
	<li>English</li>
	<li>Deutsch</li>
	<li>Français</li>
	<li><a href="http://webcodingplace.com/plugin-feature-request/">Request Your Language</a></li>
</ul>

<h3>Features</h3>
<ol>
	<li>Custom Post Type Support</li>
	<li>Use with iLightBox</li>
	<li>Image Preview in Widget</li>
	<li>Responsive Image</li>
	<li>Use from Media</li>
	<li>Upload from PC</li>
	<li>CSS3 Hover Styles</li>
	<li>Easy to use</li>
	<li>100% Responsive for every Device and Browser</li>
	<li>Change Caption Background Color</li>
	<li>Change Caption Background Opacity</li>
	<li>Change Caption Text Color</li>
	<li>Use HTML in Caption</li>
	<li>Random Effects Option (PRO)</li>
	<li>Custom Animation Speed (PRO)</li>
	<li>60+ Hover Effects (PRO)</li>
	<li>Safari Supported (PRO)</li>
	<li>Live Preview (PRO)</li>
	<li>Key Frames Animations (PRO)</li>
	<li>24 Hours Support (PRO)</li>
</ol>

<h3>Hover Styles</h3>
<ol>
	<li>Top to bottom</li>
	<li>Bottom to top</li>
	<li>Left to right</li>
	<li>Right to left</li>
	<li>Image flip up</li>
	<li>Image flip down</li>
	<li>Image flip right</li>
	<li>Image flip Left</li>
	<li>Rotate image</li>
	<li>Image turn around</li>
	<li>Zoom in</li>
	<li>Image tilt</li>
	<li>Morph</li>
	<li>Move image right</li>
	<li>Move image left</li>
	<li>Move image top</li>
	<li>Move image bottom</li>
	<li>Image squeez right</li>
	<li>Image squeez left</li>
	<li>Image squeez top</li>
	<li>Image squeez bottom</li>
</ol>

== Installation ==

1. Go to plugins in your dashboard and select 'add new'
2. Search for 'Image Caption Hover' and install it
3. Go to Appearance > Widgets, and drag the 'Image Caption Hover' to your desired widget
4. Upload image and fill some additional informations. (Not Necessary)
5. Now visit your site



== Screenshots ==

1. Easy to use interface.
2. Image Without Hover.
3. Image With Hover.

== Changelog ==

= 5.9 =
* Feature Added: Caption Link Option
* Feature Added: Caption Link Target

= 5.8 =
* Some Major Bug Fixes

= 5.7 =
* Feature Added: Role base manage
* Feature Added: Grid Builder for Pro

= 5.6 =
* Bug Fixed: Undefined is not a function
* Pro version: only for $10

= 5.5 =
* Bug Fixed: Images overlaps
* Pro Version Link Included

= 5.4 =
* Bug Fixed: Caption Center absolute positioning

= 5.3 =
* Bug Fixed: Preview in admin is now same as actual

= 5.2 =
* Bug Fixed: Now use links and raw HTML in Caption Box

= 5.1 =
* Feature Added: Use with iLightBox

= 5.0 =
* Feature Added: Use Shortcode to use in Pages, Posts
* Feature Added: Caption aligned centered vertically and horizontally
* Feature Added: German Translation Added
* Feature Added: French Translation Added

= 4.3 =
* Bug Fixed: Undefined Variables
* Bug Fixed: Double Color Pickers

= 4.2 =
* Now Responsive on Window Resize

= 4.1 =
* Some bugs removed

= 4.0 =
* 20+ hover effects added
* Change caption background color
* Change caption text color
* Change caption opacity

= 3.1 =
* Sometimes image dont show, bug fixed
* IE Support added
* More simple interface

= 3.0 =
* More Simple.
* Caption Alignment Option.
* More Hover Styles.
* Link Option Removed (You can use tags in description for linking)

= 2.0 =
* Now 100% Responsive.
* Widget preview bug fixed.
* Multiple images conflict fixed.
* Title will show also with the link.
* Auto width, height of caption as of image
* New media uploader added

== Upgrade Notice ==

= 2.0 =
* Now 100% Responsive.
* Widget preview bug fixed.
* Multiple images conflict fixed.
* Title will show also with the link.
* Auto width, height of caption as of image
* New media uploader added