<?php
/**
 *
 * @package Sydney
 */
?>


	<?php //Set widget areas classes based on user choice
		$widget_areas = get_theme_mod('footer_widget_areas', '3');
		if ($widget_areas == '3') {
			$cols = 'col-md-4';
		} elseif ($widget_areas == '4') {
			$cols = 'col-md-3';
		} elseif ($widget_areas == '2') {
			$cols = 'col-md-6';
		} else {
			$cols = 'col-md-12';
		}
	?>

	<div id="sidebar-footer" class="footer-widgets widget-area" role="complementary">
		<div class="container">
			<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
				<div class="sidebar-column col-md-12">
					<?php dynamic_sidebar( 'footer-1'); ?>
                    <div>
                    <p class="footerlink"><a href="http://www.facebook.com/"><img src= "wp-content/themes/sydney/images/fb.png" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.instagram.com/"><img src= "wp-content/themes/sydney/images/insta.png" /></a>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<a href="http://www.twitter.com/"><img src= "wp-content/themes/sydney/images/twitter.png" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.linkedin.com/"><img src= "wp-content/themes/sydney/images/linked.png" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.youtube.com/"><img src= "wp-content/themes/sydney/images/youtube.png" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                </div>
			<?php endif; ?>	
			<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
				<div class="sidebar-column <?php echo $cols; ?>">
					<?php dynamic_sidebar( 'footer-2'); ?>
                </div>
                    
				</div>
			<?php endif; ?>	
			<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
				<div class="sidebar-column <?php echo $cols; ?>">
					<?php dynamic_sidebar( 'footer-3'); ?>
				</div>
			<?php endif; ?>	
			<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
				<div class="sidebar-column <?php echo $cols; ?>">
					<?php dynamic_sidebar( 'footer-4'); ?>
				</div>
			<?php endif; ?>	
		</div>	
	</div>