<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Sydney
 */
?>
<style>
.footerlink
{
   text-align:center;
   margin-top:-20px;
   margin-bottom:20px;
    color:white;
    
}
</style>
</div>
		</div>
	</div><!-- #content -->

	<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
		<?php get_sidebar('footer'); ?>
	<?php endif; ?>

    <a class="go-top"><i class="fa fa-angle-up"></i></a>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
            <p class="footerlink"><a href="http://localhost/scooby/?post_type=product">Shop</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<a href="http://localhost/scooby/?page_id=213">Feed Calculator</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<a href="http://localhost/scooby/?page_id=223">Blog</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<a href="http://localhost/scooby/?page_id=326">Referral Rewards</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<a href="http://localhost/scooby/?page_id=95">Terms&amp;Conditions</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<a href="http://localhost/scooby/?page_id=13">My Account</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<a href="http://localhost/scooby/?page_id=13&customer-logout">Signout</a></p>
			<a href="<?php echo esc_url( __( 'http://www.etangerine.org/', 'India' ) ); ?>"><?php printf( __( '@Proudly powered by eTangerine', 'India' ), 'eTangerine' ); ?>
			<span class="sep"> | </span>
			<?php printf('All Rights Reserved'); ?></a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
