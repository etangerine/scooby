<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Sydney
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area col-md-3" role="complementary" style="background-color:rgba(51, 122, 183, 0.47); padding-bottom:300px;">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->
