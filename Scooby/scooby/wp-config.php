<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'scooby');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kk!>f+}21+)4:Ir.Cp@b]`Y8BWzbRr||fxN!>-p,B+C-wB]i~.{>$0BH,~gI`f3=');
define('SECURE_AUTH_KEY',  'FY$zq!Y~0|m,HJwe|uIvP8,(B$qxfK-sv[e{+mF~6Dof4p]!,f++6PpXQ@0H8k:)');
define('LOGGED_IN_KEY',    '-NfLt>O*`81M/Z_c=i>%k%avOf[m%p|d(W$A,Lu@6K,l2g?L~C>~C._^E$*G-D1A');
define('NONCE_KEY',        '-U>W<]rNO9uVyY-:vBA+ysJ$I%pz#!a[XsWsPQ*<%?zfO.7^|Y&y`F}@b*3h*A+o');
define('AUTH_SALT',        '%z||3+f9~kd4:1Z|(#*x}!9JD+r]7S{q23Icn;s[&ft:XDPv,!^1.X1Sp2l3G{AJ');
define('SECURE_AUTH_SALT', 'N@hZ=VNNe:*B+}>o(@xFq-55_C$9M.xSqeg+!ZhED0?ZU+A!N]Jl*}:r6L$yntUZ');
define('LOGGED_IN_SALT',   '.|4<U?H|L2?}xQC~1%OEpmck~Os.&3yj:Y-LcX3u_Qjyvdkp+mbq}++akR-1p(S1');
define('NONCE_SALT',       '`).[w7pPCXE&@A^V5PBMB{,IhvvGqu6//x<0Zts-,!*a+#+f%2,5H#$ke1&=$~n4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
